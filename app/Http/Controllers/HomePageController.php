<?php

namespace App\Http\Controllers;
use App\Categorie;
use App\Slider;
use App\Client;
use Illuminate\Http\Request;

class HomePageController extends Controller
{
	public function getAllCategories()
    {
    	$categories=Categorie::all();
    	return $categories;
    }

    public function getAllSliders()
    {
    	$sliders=Slider::where('isActive','1')->get();
    	return $sliders;
    }

    public function getAllClients()
    {
    	$clients=Client::all();
    	return $clients;
    }

    public function getReady()
    {
    	$categories=$this->getAllCategories();
    	$sliders=$this->getAllSliders();
    	$clients=$this->getAllClients();
    	return view('index',['categories'=>$categories,'sliders'=>$sliders,'clients'=>$clients]);
    }

}
