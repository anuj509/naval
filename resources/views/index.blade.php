<!DOCTYPE html>
<html>
<head>
<title>Naval Services</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Exchange Education a Responsive Web Template, Bootstrap Web Templates, Flat Web Templates, Android Compatible Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- css files -->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/chromagallery.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style1.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.css" rel="stylesheet"> 
<link href="css/easy-responsive-tabs.css" rel='stylesheet' type='text/css'/>
<!-- /css files -->
<!-- fonts -->
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Viga' rel='stylesheet' type='text/css'>
<!-- /fonts -->
<!-- js files -->
<script src="js/modernizr.custom.js"></script>
<!-- /js files -->
</head>
<body id="index.html" data-spy="scroll" data-target=".navbar" data-offset="60">
<!-- Top Bar -->
<section class="top-bar">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<a href="#index.html" class="logo"><h1>Naval</h1></a>
			</div>
			
		</div>
	</div>
</section>
<!-- /Top Bar -->
<!-- Navigation Bar -->
<div class="navbar-wrapper">
    <div class="container">
		<nav class="navbar navbar-default">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
				  <ul class="nav navbar-nav menu__list">
					<li class="active menu__item menu__item--current"><a class="menu__link" href="index.html">Home <span class="sr-only">(current)</span></a></li>
					
					<li class=" menu__item"><a class="menu__link" href="#about">About Us</a></li>
					<li class="dropdown menu__item">
						<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Products<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<div class="agile_inner_drop_nav_info">
									
									<div class="col-sm-3 multi-gd-img">
										<ul class="multi-column-dropdown">
											@foreach($categories as $category)
											<li><a href="mens.html">{{ $category->category_name }}</a></li>
											@endforeach							
										</ul>
									</div>
									
									<div class="clearfix"></div>
								</div>
							</ul>
					</li>
					
					<li class="menu__item dropdown">
					   <a class="menu__link" href="#services" class="dropdown-toggle" data-toggle="dropdown">Our Services</a>
					</li>
					<li class=" menu__item"><a class="menu__link" href="#clients">Our Clients</a></li>
					<li class=" menu__item"><a class="menu__link" href="#contact">Contact</a></li>
				  </ul>
				</div>
			  </div>
			</nav>
    </div>
</div>
<!-- /Navigation Bar -->
<!-- Banner Section -->
 <!-- Carousel
    ================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        @foreach($sliders as $k=>$slider)
        <div class="item {{ ($k==0)? 'active':'' }}">
			<img class="{{ ($k==0)? 'first':($k==1)? 'second':'third' }}-slide" src="{{ $slider->image }}" alt="{{ ($k==0)? 'first':($k==1)? 'second':'third' }} slide">
			<div class="container">
				<div class="carousel-caption">
              
				</div>
			</div>
        </div>
        @endforeach
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div><!-- /.carousel -->
<!-- /Banner Section -->
<!-- About Section -->
<section class="about-us" id="why">
	<h3 class="text-center slideanim">Why Choose Us</h3>
	<p class="text-center slideanim">We are masters of this field</p>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6">
				<div class="about-details">
					<div class="row">
						<div class="col-sm-2 col-xs-12">
							<img src="images/icon-1.png" class="img-responsive slideanim" alt="about-img">
						</div>
						<div class="col-sm-10 col-xs-12">						
							<div class="about-info slideanim">
								<p>Authorized deals with the end customers through single channel of distribution for sales , installation, maintenance and after sale services of your products.</p>
							</div>
						</div>
					</div>						
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<div class="about-details">
					<div class="row">
						<div class="col-sm-2 col-xs-12">
							<img src="images/icon-2.png" class="img-responsive slideanim" alt="about-img">
						</div>	
						<div class="col-sm-10 col-xs-12">
							<div class="about-info slideanim">
								<p>NAVAL is a well known name in the market and provides services to many customers,
for the goods brought from parallel market or goods sold outside the normal
distribution channel.
The spare parts or replacement parts business of your company will grow through our
services.</p>
							</div>
						</div>
					</div>		
				</div>
			</div>
		</div>
		<div class="row below">
			<div class="col-lg-6 col-md-6">
				<div class="about-details">
					<div class="row">
						<div class="col-sm-2 col-xs-12">
							<img src="images/icon-3.png" class="img-responsive slideanim" alt="about-img">
						</div>
						<div class="col-sm-10 col-xs-12">
							<div class="about-info slideanim">
								<p>Target major customers of South Mumbai through the Goodwill of Naval that can be used by you.</p>
							</div>
						</div>
					</div>		
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<div class="about-details">
					<div class="row">
						<div class="col-sm-2 col-xs-12">
							<img src="images/icon-4.png" class="img-responsive slideanim" alt="about-img">
						</div>
						<div class="col-sm-10 col-xs-12">
							<div class="about-info slideanim">
								<p>Regular Business and satisfied customers of your products through our services.</p>
							</div>
						</div>
					</div>		
				</div>
			</div>
		</div>	
	</div>
</section>
<!-- /About Section -->	
<!-- Our Services -->
<section class="our-services" id="services">
	<h3 class="text-center slideanim">Our Services</h3>
	<p class="text-center slideanim">Sales, Consultancy, Installations, maintenance and Chip Level repairs of the following household and industrial electronic appliance :</p>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6">
				<div class="serv-details">
					<div class="row">
						<div class="col-sm-6 col-xs-6">
							<img src="images/Plasma-TV.png" alt="" class="img-responsive slideanim">
						</div>
							
					</div>	
				</div>
				<div class="serv-info slideanim">
					<p>LCD’s, Projectors, Plasmas</p>
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<div class="serv-details">
					<div class="row">
						<div class="col-sm-6 col-xs-6">
							<img src="images/amplifier.jpg" alt="" class="img-responsive slideanim">
						</div>
						
					</div>
				</div>	
				<div class="serv-info slideanim">
					<p>LPRO Amp, All brands of amplifier, Hi-end audio including B&O, Nakamichi , Marantz range.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-md-6">
				<div class="serv-details">
					<div class="row">
						<div class="col-sm-6 col-xs-6">
							<img src="images/conference.jpeg" alt="" class="img-responsive slideanim">
						</div>
						
					</div>
				</div>		
				<div class="serv-info slideanim">
					<p>Audio / Video units for conference rooms eg :- projectors, plasma etc.</p>
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<div class="serv-details">
					<div class="row">
						<div class="col-sm-6 col-xs-6">
							<img src="images/farmhouse.jpg" alt="" class="img-responsive slideanim">
						</div>
						
					</div>
				</div>		
				<div class="serv-info slideanim">
					<p>Electronic appliances for Office , Factory and Farmhouse.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-md-6">
				<div class="serv-details">
					<div class="row">
						<div class="col-sm-6 col-xs-6">
							<img src="images/microprocessor.jpg" alt="" class="img-responsive slideanim">
						</div>
						
					</div>
				</div>		
				<div class="serv-info slideanim">
					<p>Industrial Circuits – Microprocessor & Digital based CKT, A/C ckt for indoor unit as well as A/C
plant.</p>
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<div class="serv-details">
					<div class="row">
						<div class="col-sm-6 col-xs-6">
							<img src="images/miniroom.jpg" alt="" class="img-responsive slideanim">
						</div>
						
					</div>
				</div>		
				<div class="serv-info slideanim">
					<p>Mini Video Rooms – Complete with High HD Projectors , Mechanised Screens – Pro Amps –
Speakers.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-md-6">
				<div class="serv-details">
					<div class="row">
						<div class="col-sm-6 col-xs-6">
							<img src="images/stereo.jpg" alt="" class="img-responsive slideanim">
						</div>
						
					</div>
				</div>		
				<div class="serv-info slideanim">
					<p>Hi-Fi Gadgets – portable HARD drive based Media Centre, all range of MP3 players.</p>
				</div>
			</div>
			</div>
	</div>
</section>
<!-- /Our Services -->
<!-- Our Information -->
<section class="our-info" id="about">
	<h3 class="text-center slideanim">About Us</h3>
	<p class="text-center slideanim"></p>
	<div class="container">
		<div class="row info-part">
			<div class="col-lg-6 col-md-6 col-sm-6 info-specs">
				<div class="info-details slideanim">
					<h4>Naval Services, the best you can get</h4>
					<p>Naval Services has consistently grown from past 22 years in the service industry. The company provides
par excellence
We provide services all across Mumbai and especially in South Mumbai (Colaba, Worli, Marine lines etc). We have a dedicated team of engineer’s expertise in sales; installation and maintenance of the Audio visual Appliances and digital gadgets of all kinds. We have handled large contracts and are extremely equipped regarding the same.</p>
					
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 info-specs">
				<img src="images/info-img.jpg" alt="" class="img-responsive main-img slideanim">
			</div>
		</div>
		
	</div>
</section>
<!-- /Our Information -->
<!-- Our Gallery -->
<section class="our-gallery" id="clients">
	<h3 class="text-center slideanim">Our Clients</h3>
	<p class="text-center slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> 
	<div class="container">
		<div class="content slideanim">
			<div class="chroma-gallery mygallery">
				@foreach($clients as $client)
				<img src="{{ $client->client_image }}" alt="{{ $client->client_name }}" data-largesrc="{{ $client->client_image }}">
				@endforeach
			</div>
		</div>
	</div>
</section>
<!-- /Our Gallery -->
<!-- Our Curriculum -->
<!-- /Our Curriculum -->
<!-- Google Map -->
<section class="map">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 slideanim">
				<iframe class="googlemaps" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d380510.6741687111!2d-88.01234121699822!3d41.83390417061058!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1455598377120" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>	
		</div>
	</div>
</section>
<!-- /Google Map -->
<!-- Contact Section -->
<section class="our-contacts" id="contact">
	<h3 class="text-center slideanim">Contact Us</h3>
	<p class="text-center slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<form role="form">
					<div class="row">
						<div class="form-group col-lg-4 slideanim">
							<input type="text" class="form-control user-name" placeholder="Your Name" required/>
						</div>
						<div class="form-group col-lg-4 slideanim">
							<input type="email" class="form-control mail" placeholder="Your Email" required/>
						</div>
						<div class="form-group col-lg-4 slideanim">
							<input type="tel" class="form-control pno" placeholder="Your Phone Number" required/>
						</div>
						<div class="clearfix"></div>
						<div class="form-group col-lg-12 slideanim">
							<textarea class="form-control" rows="6" placeholder="Your Message" required/></textarea>
						</div>
						<div class="form-group col-lg-12 slideanim">
							<button type="submit" href="#" class="btn-outline1">Submit</button>
						</div>
					</div>
				</form>
			</div>	
		</div>
	</div>
</section>
<!-- /Contact Section -->
<!-- Footer Section -->
<section class="footer">
	<h2 class="text-center">THANKS FOR VISITING US</h2>
	<hr>
	<div class="container">
		<div class="row">
			<div class="col-lg-4 footer-left">
				<h4>Contact Information</h4>
				<div class="contact-info">
					<div class="address">	
						<i class="glyphicon glyphicon-globe"></i>
						<p class="p3">77 Jack Street</p>
						<p class="p3">Chicago, USA</p>
					</div>
					<div class="phone">
						<i class="glyphicon glyphicon-phone-alt"></i>
						<p class="p4">+00 1010101010</p>
					</div>
					<div class="email-info">
						<i class="glyphicon glyphicon-envelope"></i>
						<p class="p4"><a href="mailto:email2@example.com">email2@example.com</a></p>
					</div>
				</div>
			</div><!-- col -->
			<div class="col-lg-4 footer-center">
				<h4>Newsletter</h4>
				<p>Register to our newsletter and be updated with the latests information regarding our services, offers and much more.</p>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label for="inputEmail1" class="col-lg-4 control-label"></label>
						<div class="col-lg-10">
							<input type="email" class="form-control" id="inputEmail1" placeholder="Email" required>
						</div>
					</div>
					<div class="form-group">
						<label for="text1" class="col-lg-4 control-label"></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="text1" placeholder="Your Name" required>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-10">
							<button type="submit" class="btn-outline">Sign in</button>
						</div>
					</div>
				</form><!-- form -->
			</div><!-- col -->
			<div class="col-lg-4 footer-right">
				<h4>Support Us</h4>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
				<ul class="social-icons2">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
				</ul>
			</div><!-- col -->
		</div><!-- row -->
	</div><!-- container -->
	<hr>
	<div class="copyright">
		<p>© 2016 Collegiate. All Rights Reserved | Design by <a href="http://w3layouts.com" target="_blank">W3layouts</a></p>
	</div>
</section>
<!-- /Footer Section -->
<!-- Back To Top -->
<a href="#0" class="cd-top">Top</a>
<!-- /Back To Top -->
<!-- js files -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/SmoothScroll.min.js"></script>
<!-- js files for gallery -->
<script src="js/chromagallery.pkgd.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() 
		{
		    $(".mygallery").chromaGallery();
		});
	</script>
<!-- /js files for gallery -->	
<!-- Back To Top -->
<script src="js/backtotop.js"></script>
<!-- /Back To Top -->
<script>
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {

  // Store hash
  var hash = this.hash;

  // Using jQuery's animate() method to add smooth page scroll
  // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
  $('html, body').animate({
    scrollTop: $(hash).offset().top
  }, 900, function(){

    // Add hash (#) to URL when done scrolling (default click behavior)
    window.location.hash = hash;
    });
  });
})
</script>
<script>
$(window).scroll(function() {
  $(".slideanim").each(function(){
    var pos = $(this).offset().top;

    var winTop = $(window).scrollTop();
    if (pos < winTop + 600) {
      $(this).addClass("slide");
    }
  });
});
</script>
<!-- /js files -->
</body>
</html>