<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->nullable();
            $table->string('product_name',100)->nullable();
            $table->string('product_image')->default('https://cdn.yourstory.com/wp-content/uploads/2016/08/125-fall-in-love.png');
            $table->string('description')->nullable();
            $table->double('base_price');
            $table->boolean('discount_available');
            $table->double('discounted_price')->nullable();
            $table->boolean('isNew');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
